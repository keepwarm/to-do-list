// The Steps
// 1. Setup Event Listeners 
// 1a. Listen for user input (submit form by hitting enter)
// 1b. 
// 2. create new todo item and ad to list
// 3. Render new todo item (show on the page)


//set up an array where we'll place the todo list items
let todoItems = [];

//the page will be updated and show on the screen
//the 'insertAdjacentHTML() method allows us to add some HTML to the DOM using an 
//already present element as a reference point
function renderTodo(todo){
    const list = document.querySelector('.js-todo-list');
    // document.getElementById('id');
    //=== document.querySelector('#id'); querySelector can select class, id, tag //the same as css selector
    list.insertAdjacentHTML('beforeend', `
        <li class = 'todo-item' data-key = '${todo.id}'>
            <input id = '${todo.id}' type = 'checkbox' />
            <label for = '${todo.id}' class = 'tick js-tick'></label>
            <span>${todo.text}</span>
            <button class = 'delete-todo js-delete-todo'>
                <svg><use href = '#delete-icon'></use></svg>
            </button>   
        </li>
    `);
}

function addTodo(val){
    const todo = {
        text: val, //holds whatever the user types into the text input
        checked: false, //helps us know if a task has been marked completed or not
        id: Date.now() //a unique identifier for the todo item
    };

    todoItems.push(todo); //adding a todo item to our list
    console.log(todoItems);
    renderTodo(todo); // this function shows the new todo on the page
}

//toggleDone() function receives the key of the list item that was checked or unchecked
//and finds the corresponding entry in the todoItems array using the findIndex method
function toggleDone(key){
    const index = todoItems.findIndex(item => item.id === Number(key));
    todoItems[index].checked = !todoItems[index].checked;

    //the 'done' class is added or removed from the item depending on its checked status
    //this class has the effect of striking out the text and showing a checkmark in the checkboxes of completed items.
    const item = document.querySelector(`[data-key ='${key}']`);
    if (todoItems[index].checked){
        item.classList.add('done');
    } else {
        item.classList.remove('done');
    }
}

//deleteTodo() function will remove the corresponding entry in todoItems and remove the todo item from the DOM
function deleteTodo(key){
    const index = todoItems.findIndex(item => item.id === Number(key));
    if(todoItems[index].checked){
        todoItems = todoItems.filter(item => item.id !== Number(key));
        const item = document.querySelector(`[data-key = '${key}']`);
        item.remove();
    
        //select the list element
        const list = document.querySelector('.js-todo-list');
        if(todoItems.length === 0) list.innerHTML = ''; 
    } else {
        alert('this todo is not finish')
    }
}

//using 'addEventListener' to listen for the submit event on '.js-form'
//then invoke a new 'addTodo()' function when the form is submitted.
const form = document.querySelector('.js-form');
form.addEventListener('submit', event => {
    event.preventDefault(); //stop the page refresh
    const input = document.querySelector('.js-todo-input');

    const text = input.value.trim(); //remove whitespace from the beginning and end of the string
    if(text !== ''){
        addTodo(text); //call function addTodo()
        input.value = ''; //the value of the text input is cleared by setting it to an empty string
        input.focus(); //so that the user can add multiple items to the list without having to focus the input over and over again
    }
});

//detect when the delete button is clicked
const list = document.querySelector('.js-todo-list');
list.addEventListener('click', event => {
    if(event.target.classList.contains('js-tick')){
        const itemKey = event.target.parentElement.dataset.key;
        toggleDone(itemKey);
    }
    //add this 'if' block
    if(event.target.classList.contains('js-delete-todo')){
        const itemKey = event.target.parentElement.dataset.key;
        deleteTodo(itemKey);
    }
});